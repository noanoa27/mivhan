<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper; //בדרופדאון
use app\models\Category; //התווסף בגלל הדרופדאון מקטגורי
use app\models\Status; 
/* @var $this yii\web\View */
/* @var $model app\models\Post */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="post-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'body')->textarea(['rows' => 6]) ?>


      <?= $form->field($model, 'category')->dropDownList(
             ArrayHelper::map(Category::find()->asArray()->all(), 'id', 'category_name')) 
             //דרופדאון שהערכים שלו מגיעים מטבלת קטגורי
             //מוצא את כל הרשומות מטבלת קטגורי ומציג כמערך, ובגלל שאנחנו רוצים רק איידי ושם אנחנו משתמשים בפונקצית מאפ
    ?>

    <?= $form->field($model, 'author')->textInput() ?>

      <?php if (\Yii::$app->user->can('editor')) { ?>
    <?= $form->field($model, 'status')->dropDownList(
             ArrayHelper::map(Status::find()->asArray()->all(), 'id', 'status_name'))
    ?>      
    <?php } ?>  

    <?= $form->field($model, 'created_at')->textInput() ?>

    <?= $form->field($model, 'updated_at')->textInput() ?>

    <?= $form->field($model, 'created_by')->textInput() ?>

    <?= $form->field($model, 'updated_by')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
