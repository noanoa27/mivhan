<?php

use yii\db\Migration;

/**
 * Class m180622_094942_init_rbac
 */
class m180622_094942_init_rbac extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

      $auth = Yii::$app->authManager;

      $author = $auth->createRole('author');
      $auth->add($author);

      $admin = $auth->createRole('admin');
      $auth->add($admin);
              
      $editor = $auth->createRole('editor');
      $auth->add($editor);

      $auth->addChild($editor, $author);
      $auth->addChild($admin, $editor);

      $manageUsers = $auth->createPermission('manageUsers');
      $auth->add($manageUsers);

      $updatePost = $auth->createPermission('updatePost');
      $auth->add($updatePost);                    
              
     $updateOwnPost = $auth->createPermission('updateOwnPost');

      $createPost = $auth->createPermission('createPost');
      $auth->add($createPost);  
      
      $publishPost = $auth->createPermission('publishPost');
      $auth->add($publishPost); 

      $deletePost = $auth->createPermission('deletePost');
      $auth->add($deletePost);

      $rule = new \app\rbac\AuthorRule;
     $auth->add($rule);
              
      $updateOwnPost->ruleName = $rule->name;                
      $auth->add($updateOwnPost);                 
                                
              
      $auth->addChild($admin, $manageUsers);
      $auth->addChild($updateOwnPost, $updatePost);
     $auth->addChild($author, $updateOwnPost); 
      $auth->addChild($editor, $deletePost);
      $auth->addChild($editor, $publishPost); 
      $auth->addChild($author, $createPost); 
      $auth->addChild($editor, $updatePost);
      

    }


    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180622_094942_init_rbac cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180622_094942_init_rbac cannot be reverted.\n";

        return false;
    }
    */
}
