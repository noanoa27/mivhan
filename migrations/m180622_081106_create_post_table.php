<?php

use yii\db\Migration;

/**
 * Handles the creation of table `post`.
 */
class m180622_081106_create_post_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('post', [
            'id' => $this->primaryKey(),
            'title' => $this->string(),
            'body' => $this->text(),
            'category' => $this->integer(),
            'author' => $this->integer(),
            'status' => $this->string(),
            'author' => $this->integer(), //id
            'created_at' => $this->timestamp(), //לעדכן שדות בצורה אוטומטית חלק 2 
            'updated_at' => $this->timestamp(), //לעדכן שדות בצורה אוטומטית חלק 2 
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('post');
    }
}
